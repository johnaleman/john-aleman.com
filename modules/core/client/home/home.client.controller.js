'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication',
    function ($scope, Authentication) {
        // This provides Authentication context.
        $scope.authentication = Authentication;

        $scope.categoryData = [
            'Javascript',
            'CSS',
            'HTML',
            'AngularJS',
            'PHP',
            'MySQL',
            'Highcharts',
            'git'
        ]

        $scope.barSeriesData = [
            {
                name: ' ',
                data: [9, 9, 9, 6, 5, 5, 7, 9]
            }
        ]
    }
]);

(function () {
    angular
        .module('core')
        .directive('chartBar', chartBar);

    function chartBar() {
        var directive = {
            restrict: 'E',
            template: '<div class="chart" style="margin: 0 auto">not working</div>',
            scope: {
                seriesData: '=',
                categoryData: '='
            },
            link: link
        };

        return directive;

        function link(scope, el, attr, ctrl) {
            var target = angular.element(el).children(0)[0];

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: target,
                    type: 'column'
                },
                title: {
                    text: 'Technical Skills'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        colorByPoint: true
                    }
                },
                colors: [
                    '#61B3A9',
                    '#DFAE37',
                    '#BD8F5C',
                    '#CC4A49',
                ],
                xAxis: {
                    categories: scope.categoryData
                },
                yAxis: {
                    title: {
                        text: 'Level'
                    }
                },
                credits: {
                    enabled: false
                },
                series: scope.seriesData
            });

            scope.$watch("seriesData", function (newValue) {
//                chart.series[0].setData(newValue, true);
            }, true);
        }
    }
})();



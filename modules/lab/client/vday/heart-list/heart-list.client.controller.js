(function() {
    angular.module('lab.vday')
            .controller('HeartListController', HeartListController);

    HeartListController.$inject = ['$scope', '$log', '$window', '$timeout', '$interval', 'heartListResolve'];

    function HeartListController($scope, $log, $window, $timeout, $interval, heartList) {
        var vm = this;

        vm.heartList = heartList;
        vm.heartMessageList = [];
        vm.loadHearts = loadHearts;
        vm.loadUserHearts = loadUserHearts;
        vm.startHeartInterval = startHeartInterval;
        vm.stopHeartInterval = stopHeartInterval;
        vm.isUserHeartLoadComplete = isUserHeartLoadComplete;
        vm.getFirstChar = getFirstChar;
        vm.handleUserHeartClick = handleUserHeartClick;

        var heartInterval;
        var heartFillColor = '#EA4D89';
        var fadeInDelayMin = 500;
        var fadeInDelayMax = 5000;
        var fadeInUserDelayMin = 500;
        var fadeInUserDelayMax = 5000;
        var fadeOutUserHeartDelay = 10 * 1000;
        var intervalDelay = 5000;
        var $heartContainer = angular.element('#heart-container');
        var $messageContainer = angular.element('#message-container');
        var $metaImage = angular.element('meta[property="og:image"]');

        initConfig(); // listeners, meta image for facebook
        init();

        function initConfig() {
            // update meta image
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
            }
            $metaImage.attr('content', window.location.origin + '/modules/lab/client/vday/assets/heart_thumb.png');

            // register listeners
            // When the $scope is destroyed, we have to make sure to teardown
            // the event binding so we don't get a leak.
            $scope.$on( "$destroy", handleDestroy );

            // TODO: implement directive see credit: http://www.bennadel.com/blog/2934-handling-window-blur-and-focus-events-in-angularjs.htm
            angular.element( $window ).on( "focus", handleFocus );
            angular.element( $window ).on( "blur", handleBlur );
        }


        function init() {
            vm.loadHearts(5);
            vm.startHeartInterval();
        }

        function loadHearts(numHearts) {
            var numHearts = numHearts | 10;

            for (var i = 0; i < numHearts; i++) {
                $heartContainer.append(getHeartHtml());
                var $last = $heartContainer.find('.heart-holder:last');
                var delay = getRandomInt(fadeInDelayMin, fadeInDelayMax);
                fadeInOut($last, delay, delay, true, function() {
                    // complete
                });
            }
        }

        function loadUserHearts(index) {
            if(angular.isDefined(index)) {
                setRandomUserHeartPosByIndex(index);
                var targetUserHeart = angular.element('#user-heart-' + index);
                var delay = getRandomInt(fadeInUserDelayMin, fadeInUserDelayMax);
                fadeInOut(targetUserHeart, delay, fadeOutUserHeartDelay, false, function(params) {
                    // complete
                    vm.loadUserHearts(params.index);
                }, {index: index});
            }
        }

        function startHeartInterval() {
            vm.stopHeartInterval();

            if (angular.isDefined(heartInterval) === false) {
                heartInterval = $interval(function() {
                    vm.loadHearts(getRandomInt(5, 10));
                }, intervalDelay);
            }
        }

        function stopHeartInterval() {
            if (angular.isDefined(heartInterval)) {
                $interval.cancel(heartInterval);
                heartInterval = undefined;
                $log.log('stop heart interval');
            }
        }

        function isUserHeartLoadComplete(isLast) {
            if (isLast) {
                // set position
                $timeout(function () {
                    var targetUserHeart, targetHeartContent;

                    angular.forEach(vm.heartList, function (heart, index) {
                        setRandomUserHeartPosByIndex(index);
                        targetUserHeart = angular.element('#user-heart-' + index);
                        targetHeartContent = targetUserHeart.find('.heart-content');

                        if (getLuminance(heart.color) > 190) {
                            // light
                            targetHeartContent.addClass('dark');
                        }

                        var delay = getRandomInt(100, 100);
                        fadeInOut(targetUserHeart, delay, 3000, false, function (params) {
                            // complete
                            vm.loadUserHearts(params.index);
                        }, {index: index});
                    });
                });
            }
        }

        // credit: http://stackoverflow.com/questions/12043187/how-to-check-if-hex-color-is-too-black
        function getLuminance(c) {
            if(angular.isDefined(c) == false) {
                return;
            }

            c = c.substring(1);      // strip #
            var rgb = parseInt(c, 16);   // convert rrggbb to decimal
            var r = (rgb >> 16) & 0xff;  // extract red
            var g = (rgb >>  8) & 0xff;  // extract green
            var b = (rgb >>  0) & 0xff;  // extract blue

            return 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709;
        }

        function getFirstChar(str) {
            if (!angular.isString(str)) {
                $log.log('ERROR: ' + str + ' must be a string');
                return;
            }

            return str.charAt(0);
        }

        function handleUserHeartClick(heart) {
            // add to message container
            $messageContainer.append(getUserHeartMessageHtml(heart));
            var $last = $messageContainer.find('.user-heart-message-holder:last');
            fadeInOut($last, 100, 3000, true, function() {
                // complete
            });
        }

        function fadeInOut($element, delayIn, delayOut, doRemove, callback, callbackParams) {
            if($element.length <= 0) {
                $log.log('ERROR: No element to fade');
                return;
            }

            var delayIn = delayIn || 500;
            var delayOut = delayOut || 500;
            var doRemove;
            if(angular.isDefined(doRemove) === false) {
                doRemove = true;
            }
            var callbackParams = callbackParams || {};

            $timeout(function() {
                // fade in
                if($element.hasClass('in') === false) {
                    $element.addClass('in');
                }

                $element.on(whichTransitionEvent(), function(e) {
                    // fade in complete
                    $element.off(whichTransitionEvent()); // clear events

                    $timeout(function() {
                        // fade out
                        $element.removeClass("in");
                        $element.on(whichTransitionEvent(), function(e) {
                            // fade out complete
                            $element.off(whichTransitionEvent()); // clear events

                            if (doRemove === true) {
                                $element.remove();
                            }

                            if(typeof(callback) === "function") {
                                callback(callbackParams);
                            }
                        });
                    }, delayOut);
                })
            }, delayIn);
        }

        function setRandomUserHeartPosByIndex(index) {
            var targetUserHeart, maxTop, maxRight, top, left, style;
            targetUserHeart = angular.element('#user-heart-' + index);
            maxTop = $heartContainer.height();
            maxRight = $heartContainer.width();
            top = getRandomInt(0, maxTop) + 'px';
            left = getRandomInt(0, maxRight) + 'px';
            style = 'top: '+top+'; left: '+left+'';

            targetUserHeart.attr('style', style);
        }

        // detect which transition event
        // credit: David Walsh: http://davidwalsh.name/css-animation-callback
        function whichTransitionEvent() {
            var t, el = document.createElement("fakeelement");
            var transitions = {
                "transition": "transitionend",
                "OTransition": "oTransitionEnd",
                "MozTransition": "transitionend",
                "WebkitTransition": "webkitTransitionEnd"
            };

            for (t in transitions) {
                if (el.style[t] !== undefined) {
                    return transitions[t];
                }
            }
        }

        function getHeartHtml(heart, index, type) {
            // color scheme: https://dribbble.com/shots/2522139-Be-my-Valenwine
            var index = index;
            var fillColor = heartFillColor;
            var maxTop = $heartContainer.height();
            var maxRight = $heartContainer.width();
            var top = getRandomInt(0, maxTop) + 'px';
            var left = getRandomInt(0, maxRight) + 'px';
            var className = 'heart-holder';

            if (type === 'user') {
                className = 'user-heart-holder';
                fillColor = heart.color;
            }

            var html = '';
            html += '<div class="'+className+' fade" style="top: '+top+'; left: '+left+';">';
            html += '   <svg id="heart" width="100%" viewBox="0 0 793 1122" xmlns="http://www.w3.org/2000/svg">';
            html += '       <title>heart</title>';
            html += '       <g>';
            html += '        <title>Layer 1</title>';
            html += '           <g id="layer1">';
            html += '               <path fill='+fillColor+' stroke="none" stroke-width="10.700001" stroke-miterlimit="4" id="path7" d="m263.41571,235.145874c-66.240005,0 -119.999954,53.76001 -119.999954,120c0,134.755524 135.933151,170.08728 228.562454,303.308044c87.574219,-132.403381 228.5625,-172.854584 228.5625,-303.308044c0,-66.23999 -53.759888,-120 -120,-120c-48.047913,0 -89.401611,28.370422 -108.5625,69.1875c-19.160797,-40.817078 -60.514496,-69.1875 -108.5625,-69.1875z"/>';
            html += '           </g>';
            html += '       </g>';
            html += '   </svg>';
            html += '</div>';
            
            return html;
        }

        function getUserHeartMessageHtml(heart) {
            var html = '';
            html += '<div class="user-heart-message-holder fade">';
            html += '   <div class="user-heart-name">'+heart.firstName.toUpperCase()+' '+vm.getFirstChar(heart.lastName).toUpperCase()+'</div>';
            html += '   <div class="user-heart-message">'+heart.message+'</div>';
            html += '</div>';
            return html;
        }

        /**
         * Returns a random integer between min (inclusive) and max (inclusive)
         * Using Math.round() will give you a non-uniform distribution!
         */
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function handleFocus(event) {
            vm.startHeartInterval();
        }

        function handleBlur(event) {
            vm.stopHeartInterval();
        }

        function handleDestroy() {
            // start destroy
            vm.stopHeartInterval();
        }
    }
})();
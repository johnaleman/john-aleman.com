(function() {
    angular
        .module('lab.vday')
        .controller('VDayFormController', VDayFormController);

    VDayFormController.$inject = ['$scope', '$log', '$state', 'heartResolve'];

    function VDayFormController($scope, $log, $state, heart) {
        var vm = this;

        var mockHeart = {
            name: 'John',
            email: 'johnaleman@gmail.com',
            color: '#ffcc00',
            message: 'Spread the love'
        }

        vm.heart = heart;
        angular.extend(vm.heart, mockHeart); // populate data

        vm.updating = false; // is heart document currently updating
        vm.handleColorOptionChange = handleColorOptionChange;

        // crud
        vm.save = save;
        vm.cancel = cancel;
        vm.remove = remove;

        // settings
        vm.form = {};
        vm.error = null;
        vm.hearColorOptions = [
            {value: '#186CAC', name: "blue"},
            {value: '#07FF00', name: "green"},
            {value: '#653392', name: "purple"},
            {value: '#FF9999', name: "pink"},
            {value: '#FF00FF', name: "bright pink"},
            {value: '#E62069', name: "dark pink"},
            {value: '#FFB701', name: "orange"},
            {value: '#FFFFFF', name: "white"},
            {value: '#FFFF99', name: "yellow"}
        ]

        // credit: http://www.rd.com/food/fun/over-10-years-of-candy-hearts-sayings-valentines-day/
        vm.heartPhraseList = [
            {value: 'RECIPE 4 LOVE', name: "RECIPE 4 LOVE"},
            {value: 'TABLE 4 TWO', name: "TABLE 4 TWO"},
            {value: 'STIR MY HEART', name: "STIR MY HEART"},
            {value: 'MY TREAT', name: "MY TREAT"},
            {value: 'TOP CHEF', name: "TOP CHEF"},
            {value: 'SUGAR PIE', name: "SUGAR PIE"},
            {value: 'HONEY BUN', name: "HONEY BUN"},
            {value: 'SPICE IT UP', name: "SPICE IT UP"},
            {value: 'YUM YUM', name: "YUM YUM"},
            {value: 'MELT MY ♥', name: "MELT MY ♥"},
            {value: 'IN A FOG', name: "IN A FOG"},
            {value: 'HEAT WAVE', name: "HEAT WAVE"},
            {value: 'SUN SHINE', name: "SUN SHINE"},
            {value: 'WILD LIFE', name: "WILD LIFE"},
            {value: 'NATURE LOVER', name: "NATURE LOVER"},
            {value: 'DO GOOD', name: "DO GOOD"},
            {value: 'COOL CAT', name: "COOL CAT"},
            {value: "PUPPY LOVE", name: "PUPPY LOVE"},
            {value: "TAKE A WALK", name: "TAKE A WALK"},
            {value: "BEAR HUG", name: "BEAR HUG"},
            {value: "TOP DOG", name: "TOP DOG"},
            {value: "URA TIGER", name: "URA TIGER"},
            {value: "GO FISH", name: "GO FISH"},
            {value: "LOVE BIRD", name: "LOVE BIRD"},
            {value: "PURR FECT", name: "PURR FECT"},
            {value: "3 WISHES", name: "3 WISHES"},
            {value: "EVER AFTER", name: "EVER AFTER"},
            {value: "NEW YOU", name: "NEW YOU"},
            {value: "MAGICNEW LOVE", name: "MAGICNEW LOVE"},
            {value: "DREAM", name: "DREAM"},
            {value: "CHARM ME", name: "CHARM ME"},
            {value: "START NOW", name: "START NOW"},
            {value: "I LOVE YOU", name: "I LOVE YOU"},
            {value: "MY GIRL", name: "MY GIRL"},
            {value: "MISS ME", name: "MISS ME"},
            {value: "IT'S LOVE", name: "IT'S LOVE"},
            {value: "LET'S KISS", name: "LET'S KISS"},
            {value: "BE TRUE", name: "BE TRUE"},
            {value: "MY LOVE", name: "MY LOVE"},
            {value: "IT'S TRUE", name: "IT'S TRUE"},
            {value: "MARRY ME", name: "MARRY ME"},
            {value: "I DO", name: "I DO"},
            {value: "I WILL", name: "I WILL"},
            {value: "I WISH", name: "I WISH"},
            {value: "HELLO", name: "HELLO"},
            {value: "BE KIND", name: "BE KIND"}
        ]

        function save(isValid, changeState) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.heartForm');
                return false;
            }

            vm.updating = true;
            if (vm.heart._id) {
                vm.heart.$update(successCallback, errorCallback).then(function(heart) {
                    vm.updating = false;
                });
            } else {
                vm.heart.$save(successCallback, errorCallback).then(function(heart) {
                    vm.heart = false;
                });
            }

            function successCallback(res) {
                if(changeState) {
                    $state.go('vday.list', {
                        heart: res
                    }, {
                        reload: true
                    });
                }
            }

            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }

        function cancel() {
            var prev = $state.previous.state.name || 'vday.list';
            $state.go(prev);
        }

        function remove() {
            $log.log('remove');
        }

        function handleColorOptionChange(color) {
            if(angular.isDefined(color) === false || color === '' || typeof color === undefined) {
                return;
            }

            var $colorPreview = angular.element('#color-preview');
            $colorPreview.css('background-color', color);
        }
    }
})();
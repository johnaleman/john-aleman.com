(function() {
    'user strict';

    angular
        .module('lab.vday')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('vday', {
                abstract: true,
                url: '/lab/vday',
                template: '<ui-view/>'
            })
            .state('vday.list', {
                url: '',
                templateUrl: 'modules/lab/client/vday/heart-list/heart-list.html',
                controller: 'HeartListController',
                controllerAs: 'vm',
                resolve: {
                    heartListResolve: getHeartList
                }
            })
            .state('vday.list.create', {
                url: '/create',
                templateUrl: 'modules/lab/client/vday/heart-form/heart-form.client.html',
                controller: 'VDayFormController',
                controllerAs: 'vm',
                resolve: {
                    heartResolve: newHeart
                }
            })
    }

    getHeartList.$inject = ['HeartService'];

    function getHeartList(HeartService) {
        return HeartService.query(function(result) {
            return result;
        }).$promise;
    }

    newHeart.$inject = ['HeartService'];

    function newHeart(HeartService) {
        return new HeartService();
    }
})();
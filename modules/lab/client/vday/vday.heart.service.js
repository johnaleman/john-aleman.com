(function() {
    'use strict';

    angular
            .module('lab.vday')
            .factory('HeartService', HeartService);

    HeartService.$inject = ['$resource'];

    function HeartService($resource) {
        return $resource('api/vday/heart/:heartId', {
            heartId: '@_id'
        }, {
            query: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            }
        });
    }
})();
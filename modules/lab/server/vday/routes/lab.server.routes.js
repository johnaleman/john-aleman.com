'use strict';

module.exports = function (app) {
    var labPolicy = require('../policies/lab.server.policy'),
        heart = require('../controllers/lab.server.controller');

    app.route('/api/vday/heart').all(labPolicy.isAllowed)
        .get(heart.list)
        .post(heart.create);

    // Single article routes
    app.route('/api/vday/heart/:heartId').all(labPolicy.isAllowed)
        .get(heart.read)
        .put(heart.update)
        .delete(heart.delete);

    // Finish by binding the article middleware
    app.param('heartId', heart.heartByID);
};

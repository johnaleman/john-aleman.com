'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    multer = require('multer'), // doc: https://github.com/expressjs/multer
    config = require(path.resolve('./config/config')),
    Heart = mongoose.model('Heart'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a heart
 */
exports.create = function (req, res) {
    var heart = new Heart(req.body);
//    portfolio.user = req.user;

    heart.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(heart);
        }
    });
};

/**
 * Show the current portfolio
 */
exports.read = function (req, res) {
    res.json(req.heart);
};

/**
 * Update an portfolio
 */
exports.update = function (req, res) {
    var heart = req.heart;
    heart.name = req.body.name;
    heart.email = req.body.email;
    heart.color = req.body.color;
    heart.message = req.body.message;

    heart.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(heart);
        }
    });
};

/**
 * Delete a heart
 */
exports.delete = function (req, res) {
    var heart = req.heart;

    heart.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(heart);
        }
    });
};

/**
 * List of Hearts
 */
exports.list = function (req, res) {
    Heart.find().sort('created').exec(function (err, heartList) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(heartList);
        }
    });
};

/**
 * Heart middleware
 */
exports.heartByID = function (req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Heart is invalid'
        });
    }

    Heart.findById(id).exec(function (err, heart) {
        if (err) {
            return next(err);
        } else if (!heart) {
            return res.status(404).send({
                message: 'No portfolio with that identifier has been found'
            });
        }
        req.heart = heart;
        next();
    });
};
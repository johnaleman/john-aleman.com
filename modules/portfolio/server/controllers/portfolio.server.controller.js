'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    multer = require('multer'), // doc: https://github.com/expressjs/multer
    config = require(path.resolve('./config/config')),
    Portfolio = mongoose.model('Portfolio'),
    errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an portfolio
 */
exports.create = function (req, res) {
    var portfolio = new Portfolio(req.body);
    portfolio.user = req.user;

    portfolio.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(portfolio);
        }
    });
};

/**
 * Update profile picture
 */
exports.changePortfolioPicture = function (req, res) {
    var user = req.user;
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, config.uploads.portfolioUpload.dest)
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname + '-' + Date.now() + '.png');
        }
    });

    // .array(fieldname[, maxCount])
    var upload = multer({storage: storage}).array('newProjectHeroImage', 10);

    var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;

    // Filtering to upload only images
    upload.fileFilter = profileUploadFileFilter;

    if (user) {
        upload(req, res, function (uploadError) {
            console.log('complete - req.files', req.files);

            if(uploadError) {
                return res.status(400).send({
                    message: 'Error occurred while uploading portfolio picture'
                });
            } else {
                var response = {};
                response['status'] = 'success';
                response['files' ] = req.files;


                res.json(response);
            }
        });
    } else {
        res.status(400).send({
            message: 'User is not signed in'
        });
    }
};

/**
 * Show the current portfolio
 */
exports.read = function (req, res) {
    res.json(req.portfolio);
};

/**
 * Update an portfolio
 */
exports.update = function (req, res) {
    var portfolio = req.portfolio;
    portfolio.order = req.body.order;
    portfolio.title = req.body.title;
    portfolio.content = req.body.content;
    portfolio.technology = req.body.technology;
    portfolio.link = req.body.link;
    portfolio.heroImageURL = req.body.heroImageURL;
    portfolio.imageListURL = req.body.imageListURL;

    portfolio.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(portfolio);
        }
    });
};

/**
 * Delete an portfolio
 */
exports.delete = function (req, res) {
    var portfolio = req.portfolio;

    portfolio.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(portfolio);
        }
    });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
    Portfolio.find().sort('order').populate('user', 'displayName').exec(function (err, portfolioList) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(portfolioList);
        }
    });
};

/**
 * Portfolio middleware
 */
exports.portfolioByID = function (req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Portfolio is invalid'
        });
    }

    Portfolio.findById(id).populate('user', 'displayName').exec(function (err, portfolio) {
        if (err) {
            return next(err);
        } else if (!portfolio) {
            return res.status(404).send({
                message: 'No portfolio with that identifier has been found'
            });
        }
        req.portfolio = portfolio;
        next();
    });
};
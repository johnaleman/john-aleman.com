'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Article Schema
 */
var PortfolioSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    order: {
        type: Number,
        default: 999,
        trim: true
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: 'Title cannot be blank'
    },
    content: {
        type: String,
        default: '',
        trim: true
    },
    technology: {
        type: String,
        default: '',
        trim: true
    },
    link: {
        type: String,
        default: '',
        trim: true
    },
    heroImageURL: {
        type: String,
        default: 'modules/users/client/img/profile/default.png',
        trim: true
    },
    imageListURL: {
        type: Array,
        default: []
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Portfolio', PortfolioSchema);
'use strict';

module.exports = function (app) {
    var portfolioPolicy = require('../policies/portfolio.server.policy'),
        portfolio = require('../controllers/portfolio.server.controller');

    app.route('/api/portfolio').all(portfolioPolicy.isAllowed)
        .get(portfolio.list)
        .post(portfolio.create);

    app.route('/api/portfolio/picture').post(portfolio.changePortfolioPicture);

    // Single article routes
    app.route('/api/portfolio/:portfolioId').all(portfolioPolicy.isAllowed)
        .get(portfolio.read)
        .put(portfolio.update)
        .delete(portfolio.delete);

    // Finish by binding the article middleware
    app.param('portfolioId', portfolio.portfolioByID);
};

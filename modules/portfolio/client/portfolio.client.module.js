(function (app) {
    'use strict';

    app.registerModule('portfolio', ['core', 'ngSanitize', 'summernote']);
    app.registerModule('portfolio.services');
    app.registerModule('portfolio.routes', ['ui.router', 'portfolio.services']);
})(ApplicationConfiguration);
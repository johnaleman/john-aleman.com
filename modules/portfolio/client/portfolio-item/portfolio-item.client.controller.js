(function() {
    'use strict';

    angular
        .module('portfolio')
        .controller('PortfolioGalleryController', PortfolioGalleryController);

    PortfolioGalleryController.$inject = ['$log', '$scope', '$window', '$timeout', '$interval', 'portfolioResolve', 'Authentication']

    function PortfolioGalleryController($log, $scope, $window, $timeout, $interval, portfolio, Authentication) {
        var vm = this;
        vm.portfolio = portfolio;
        vm.authentication = Authentication;
        vm.launchProject = launchProject;
        vm.setActiveThumbByIndex = setActiveThumbByIndex;
        vm.pauseGalleryInterval = pauseGalleryInterval;
        vm.startGalleryInterval = startGalleryInterval;
        vm.stopGalleryInterval = stopGalleryInterval;
        vm.incGalleryIndex = incGalleryIndex;

        var galleryInterval;
        var galleryIntervalDelay = 5 * 1000;
        var galleryStopTimeout = 5 * 1000;
        var currentGalleryIndex = 0;
        vm.isPaused = true;

        vm.startGalleryInterval();

        function launchProject() {
            $window.open(vm.portfolio.link, '_blank');
        }

        function startGalleryInterval() {
            vm.stopGalleryInterval(); // stops any running interval to avoid two intervals running at the same time
            vm.isPaused = false;

            galleryInterval = $interval(function() {
                $log.log('tick');
                vm.incGalleryIndex();
            }, galleryIntervalDelay);
        }

        function stopGalleryInterval() {
            if (angular.isDefined(galleryInterval)) {
                $interval.cancel(galleryInterval);
                vm.isPaused = true;
                galleryInterval = undefined;
            }
        }

        function pauseGalleryInterval() {
            if(vm.isPaused === false) {
                stopGalleryInterval();
            } else {
                incGalleryIndex();
                startGalleryInterval();
            }
        }

        function incGalleryIndex() {
            if (currentGalleryIndex < vm.portfolio.imageListURL.length - 1) {
                currentGalleryIndex++;
            } else {
                currentGalleryIndex = 0;
            }

            vm.setActiveThumbByIndex(currentGalleryIndex);
        }

        function setActiveThumbByIndex($index, $event) {
            currentGalleryIndex = $index;
            setActiveGalleryBtn(currentGalleryIndex);
            setActiveImageHolder(currentGalleryIndex);

            if (vm.isPaused === false) {
                vm.startGalleryInterval();
            }

            /*if (angular.isDefined($event)) {
                vm.stopGalleryInterval();

                $timeout(function() {
                    vm.startGalleryInterval();
                }, galleryStopTimeout);
            }*/
        }

        function setActiveGalleryBtn($index) {
            var targetBtn = angular.element('.gallery-button[data-index="'+$index+'"]');
            angular.element('.gallery-button').removeClass('active');
            targetBtn.addClass('active');
        }

        function setActiveImageHolder($index) {
            var targetImageHolder = angular.element('.image-holder[data-index="'+$index+'"]');
            if (targetImageHolder.length > 0) {
                angular.element('.image-holder').removeClass('active'); // remove all active classes
                targetImageHolder.addClass('active');
            }
        }

        $scope.$on('$destroy', function() {
            vm.stopGalleryInterval();
        });
    }
})();
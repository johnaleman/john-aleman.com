(function () {
    'use strict';

    angular
        .module('portfolio')
        .controller('PortfolioFormController', PortfolioFormController);

    PortfolioFormController.$inject = ['$scope', '$state', '$timeout', '$interval', 'portfolioResolve', 'Authentication'];

    function PortfolioFormController($scope, $state, $timeout, $interval, portfolio, Authentication) {
        var vm = this;

        vm.portfolio = portfolio;
        vm.authentication = Authentication;
        vm.heroMultiSelect = false;
        vm.thumbsMultiSelect = true;
        vm.updating = false; // is portfolio document currently updating
        vm.error = null;
        vm.form = {};
        vm.onSuccessHeroItem = onSuccessHeroItem;
        vm.removeHeroItem = removeHeroItem;
        vm.onSuccessImageListItem = onSuccessImageListItem;
        vm.removeImageListItemByURL = removeImageListItemByURL;
        vm.remove = remove;
        vm.save = save;

        vm.options = {
            height: 500,
            focus: true,
            airMode: true,
            toolbar: [
                ['edit',['undo','redo']],
                ['headline', ['style']],
                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                ['fontface', ['fontname']],
                ['textsize', ['fontsize']],
                ['fontclr', ['color']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ]
        };

        function onSuccessHeroItem(fileItem, response, status, headers) {
            console.log('onSuccessHeroItem');
            if(vm.updating === true) {
                // try again
                var delay = 1000;
                $timeout(function() {
                    vm.onSuccessHeroItem(fileItem, response, status, headers);
                }, delay);
            } else {
                vm.portfolio.heroImageURL = response.files[0].path;
                save(vm.form.portfolioForm.$valid, false);
            }
        }

        function removeHeroItem() {
            if (confirm('Are you sure you want to delete?')) {
                vm.portfolio.heroImageURL = '';
            }

            save(vm.form.portfolioForm.$valid, false);
        }

        function onSuccessImageListItem(fileItem, response, status, headers) {
            console.log('onSuccessImageListItem');
            // if currently updating
            if(vm.updating === true) {
                // try again
                var delay = 1000;
                $timeout(function() {
                    vm.onSuccessImageListItem(fileItem, response, status, headers);
                }, delay);
            } else {
                vm.portfolio.imageListURL.push(response.files[0].path);
                save(vm.form.portfolioForm.$valid, false);
            }
        }

        function removeImageListItemByURL(itemURL) {
            var index = vm.portfolio.imageListURL.indexOf(itemURL);
            if (index > -1 && confirm('Are you sure you want to delete?')) {
                vm.portfolio.imageListURL.splice(index, 1);
            }

            save(vm.form.portfolioForm.$valid, false);
        }

        // remove existing portfolio document
        function remove() {
            if (confirm('Are you sure you want to delete?')) {
                vm.portfolio.$remove($state.go('portfolio.list'));
            }
        }

        // save portfolio document
        function save(isValid, changeState) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.portfolioForm');
                return false;
            }

            vm.updating = true;
            if (vm.portfolio._id) {
                vm.portfolio.$update(successCallback, errorCallback).then(function(portfolio) {
                    vm.updating = false;
                });
            } else {
                vm.portfolio.$save(successCallback, errorCallback).then(function(portfolio) {
                    vm.updating = false;
                });
            }

            function successCallback(res) {
                if(changeState) {
                    $state.go('portfolio.view', {
                        portfolioId: res._id
                    });
                }
            }

            function errorCallback(res) {
                vm.error = res.data.message;
            }
        }
    }
})();

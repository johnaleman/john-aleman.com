(function () {
    'use strict';

    angular
        .module('portfolio')
        .controller('PortfolioListController', PortfolioListController);

    PortfolioListController.$inject = ['$log', 'Authentication', 'PortfolioService'];

    function PortfolioListController($log, Authentication, PortfolioService) {
        var vm = this;

        if (Authentication.user !== '') {
            vm.user = Authentication.user;
        }

        vm.isUserDefined = isUserDefined;

        PortfolioService.query(function(result) {
            vm.portfolioList = result;
        });

        function isUserDefined() {
            return angular.isDefined(vm.user);
        }
    }
})();
(function () {
    'use strict';

    angular
        .module('portfolio.services')
        .factory('PortfolioService', PortfolioService);

    PortfolioService.$inject = ['$resource'];

    function PortfolioService($resource) {
        return $resource('api/portfolio/:portfolioId', {
            portfolioId: '@_id'
        }, {
            query: {
              method: 'GET',
              isArray: true
            },
            update: {
                method: 'PUT'
            }
        });
    }
})();

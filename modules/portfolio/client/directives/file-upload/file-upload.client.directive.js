// doc: https://github.com/nervgh/angular-file-upload/wiki/Module-API
angular
    .module('portfolio')
    .directive('fileUpload', fileUpload);

function fileUpload() {
    var directive = {
        restrict: 'EA',
        templateUrl: 'modules/portfolio/client/directives/file-upload/file-upload.client.html',
        // TODO: remove files, thumbnail properties
        scope: {
            onSuccessItem: '=?',
            multiSelect: '=?'
        },
        link: link,
        controller: controller,
        controllerAs: 'vm',
        bindToController: true // because the scope is isolated
    };
    return directive;

    function link(scope, element, attrs, vm) {
        vm.header = attrs.header || 'Select Image(s)';
    }

    controller.$inject = ['FileUploader'];
    function controller(FileUploader) {
        var vm = this;

        // Create file uploader instance
        vm.uploader = new FileUploader({
            url: 'api/portfolio/picture',
            alias: 'newProjectHeroImage'
        });

        // Set file uploader image filter
        vm.uploader.filters.push({
            name: 'imageFilter',
            fn: function (item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS
        vm.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        vm.uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);

            // if not multi select then remove old item from uploader
            if (vm.multiSelect === false && vm.uploader.queue.length > 1) {
                vm.uploader.queue.shift();
            }
        };
        vm.uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        vm.uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        vm.uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        vm.uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        vm.uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
            vm.onSuccessItem(fileItem, response, status, headers);
        };
        vm.uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        vm.uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        vm.uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        vm.uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };
    }
}
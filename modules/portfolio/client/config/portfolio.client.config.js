(function () {
    'use strict';

    angular
        .module('portfolio')
        .run(menuConfig);

    menuConfig.$inject = ['Menus'];

    function menuConfig(Menus) {
        Menus.addMenuItem('topbar', {
            title: 'Portfolio',
            state: 'portfolio.list',
            type: 'item',
            roles: ['*']
        });
    }
})();
(function () {
    'use strict';

    angular
        .module('portfolio.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('portfolio', {
                abstract: true,
                url: '/portfolio',
                template: '<ui-view/>'
            })
            .state('portfolio.list', {
                url: '',
                templateUrl: 'modules/portfolio/client/portfolio-list/portfolio-list.client.view.html',
                controller: 'PortfolioListController',
                controllerAs: 'vm'
            })
            .state('portfolio.list.view', {
                url: '/{portfolioId:[a-zA-Z0-9]{10,}}', // only match letters and numbers > 10
                templateUrl: 'modules/portfolio/client/portfolio-item/portfolio-item.client.view.html',
                controller: 'PortfolioGalleryController',
                controllerAs: 'vm',
                resolve: {
                    portfolioResolve: getPortfolio
                }
            })
            .state('portfolio.create', {
                url: '/create',
                templateUrl: 'modules/portfolio/client/form/form.client.html',
                controller: 'PortfolioFormController',
                controllerAs: 'vm',
                resolve: {
                    portfolioResolve: newPortfolio
                },
                data: {
                    roles: ['user', 'admin']
                }
            })
            .state('portfolio.edit', {
                url: '/{portfolioId:[a-zA-Z0-9]{10,}}/edit', // only match letters and numbers > 10
                templateUrl: 'modules/portfolio/client/form/form.client.html',
                controller: 'PortfolioFormController',
                controllerAs: 'vm',
                resolve: {
                    portfolioResolve: getPortfolio
                },
                data: {
                    roles: ['user', 'admin']
                }
            });
    }

    getPortfolio.$inject = ['$stateParams', 'PortfolioService'];

    function getPortfolio($stateParams, PortfolioService) {
        return PortfolioService.get({
            portfolioId: $stateParams.portfolioId
        }).$promise;
    }

    newPortfolio.$inject = ['PortfolioService'];

    function newPortfolio(PortfolioService) {
        return new PortfolioService();
    }
})();